# python-urls-monitor

This script uses urllib to get the http response of a URL given and keep the track of errors (responses not in the range of 2xx) in a log file called example.log. Optionally (with argument -t specified) it launchs a Telegram bot and send the responses in a Telegram Message to the users in the list called USERS inside the file config.ini.

Use urls_monitor -h for help
# Requerimientos
Python >= 3.8
python-telegram-bot

Se pueden instalar con el siguiente comando:
``` sh
# pip install -r requirements.txt
```

# Configuration
Inside the file config.ini there is 2 sections:  
GENERAL with the option BOT_TOKEN, here should be coppied the TOKEN given by Bot_Father
USERS while the script is running with the -t argument, the bot is alive and listen for messages, users starting the bot gets as answer their ID. In these fields should be coppied these IDs

## Example of use

urls_monitor -u https://www.google.com -t

Prints the status of the site in intervals of 2 seconds (or stablished time with -w argument).
Keeps logs of the status in example.log file and send messages with Telegram


# ToDo

Catch errors and sent them by Telegram  
Control the bot with commands: start/stop to log a new site, stop script, stop bot...
