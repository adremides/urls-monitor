#!/usr/bin/env python3

from urllib import request, error
import argparse, time, logging, sys, os
from datetime import datetime


def get_server_status_code(url):
    try:
        with request.urlopen(url, timeout=1) as response:
            response_status = response.getcode()
        return response_status
    except error.URLError as e:
        return f"ERROR: {e}"


SUCCESSFUL_RESPONSE_RANGE = range(200, 300)

logging.basicConfig(filename='example.log', level=logging.INFO)

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument(
    '-u',
    required=True,
    dest='urls',
    help='Define una o más URLs para observar',
    type=str,
    nargs='+'
)
parser.add_argument(
    '-w', '--wait',
    default=2,
    dest='wait',
    help='Tiempo de espera entre consultas',
    type=int
)
parser.add_argument(
    '-t', '--telegram-bot',
    action='store_true',
    dest='telegram_bot_on',
    help='Enviar notificación con un bot de Telegram'
)
args = parser.parse_args()

if args.telegram_bot_on:
    import bot_control

print('Started logging')

try:

    while True:
        for url in args.urls:
            status_code = get_server_status_code(url)
            # print(status_code)
            # Si se quiere loguear un error en específico, ejemplo:
            if status_code not in SUCCESSFUL_RESPONSE_RANGE:
                logging.info(f'{datetime.now()} | {url} status: {status_code}')
                if args.telegram_bot_on:
                    bot_control.spam_message(f'{datetime.now()} | {url} status: {status_code}')                
            time.sleep(args.wait)
except KeyboardInterrupt:
    print('Logging stopped')
    # sys.exit(0)

if args.telegram_bot_on:
    print('Stopping Telegram Bot ...')
    bot_control.spam_message('El script se detuvo')
    try:
        sys.exit(0)
    except SystemExit:
        os._exit(0)
