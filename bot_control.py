from telegram import Bot
from telegram.ext import Updater, CommandHandler, MessageHandler
import logging, configparser

from telegram.ext.filters import Filters


# Función para mandar mensaje a todos los usuarios
def spam_message(message):
    for _, user_id in USERS:
        BOT.send_message(chat_id=user_id, text=message)


# Función que atiende el comando start
def start(update, context):
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Colocá tu ID en el archivo config.ini para poder enviarte datos: {update.effective_chat.id}"
    )


# Funcion que atiende comandos desconocidos
def unknown(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="No es un comando válido")


# Obtener valores de las configuraciones
CONFIG = configparser.ConfigParser()
CONFIG.read('config.ini')
BOT_TOKEN = CONFIG['GENERAL']['BOT_TOKEN']
USERS = CONFIG.items('USERS')

BOT = Bot(BOT_TOKEN)


# Controles básicos
updater = Updater(
    token=BOT_TOKEN,
    use_context=True
)
dispatcher = updater.dispatcher


# Logs
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.DEBUG
)
logger = logging.getLogger()


# Manejadores de comandos
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)
unknown_handler = MessageHandler(Filters.command, unknown)
dispatcher.add_handler(unknown_handler)


# Enviar mensaje de inicio
spam_message('El script se está iniciando')

# Ejecutar el Bot
updater.start_polling()
# updater.idle() #  Esto devuelve en control al Thread

# Enviar mensaje de finalización (se hace en el script al finalizar)
# spam_message('El script se detuvo')
